package org.dabrowskipw.markovmodel;

import java.util.HashMap;
import java.util.Random;
import java.util.stream.IntStream;

public class MarkovModelExample {
	
	public static class MarkovModel {
		private HashMap<Character, Integer> states = new HashMap<Character, Integer>();
		private HashMap<Integer, Character> revStates = new HashMap<Integer, Character>();
		private int[][] transitions;
		private int[] stateFrequencies;
		private int numStates;
		
		public MarkovModel(String trainingString) {
			char[] training = trainingString.toCharArray();
			// Liste der Zustaende erstellen, jedem Zustand einen Index zuweisen (und eine
			// umgekehrte Liste Index->Zustand, hilfreich fuer simulate() weiter unten)
			for(Character c : training) {
				if(!states.containsKey(c)) {
					int stateNum = states.size();
					states.put(c, stateNum);
					revStates.put(stateNum, c);
				}
			}
			// Tabellen fuer Zustandsuebergangshaeufigkeiten und Zustandshaeufigkeiten 
			// initialisieren
			numStates = states.size();
			transitions = new int[numStates][numStates];
			stateFrequencies = new int[numStates];
			for(int i=1; i<training.length; i++) {
				// Die Anzahl der beobachteten Uebergaenge von (i-1)-ten Zeichen zum i-ten 
				// Zeichen um 1 erhoehen, dafuer die Indices der Zeichen aus der Zustandsliste 
				// holen
				transitions[states.get(training[i-1])][states.get(training[i])] += 1;
				// Fuer jeden Zustand mitzaehlen, wie haeufig er vorkommt
				stateFrequencies[states.get(training[i-1])] += 1;
			}
		}
		
		public String simulate(int length, int seed) {
			char[] res = new char[length];
			Random random = new Random(seed);
			double randval = random.nextDouble() * IntStream.of(stateFrequencies).sum();
			// Entsprechend der Zustaendshaeufigkeiten zufaellig den ersten Zustand auswaehlen
			for(int selectedPos = 0; selectedPos<numStates && randval > 0; selectedPos++) {
				randval -= stateFrequencies[selectedPos];
				if(randval <= 0) {
					res[0] = revStates.get(selectedPos); 
				}
			}
			// Fuer jede naechste Position: Drauffolgendes Zeichen zufaellig entsprechend
			// der beobachteten Uebergangshaeufigkeiten auswaehlen.
			for(int pos = 1; pos < length; pos++) {
				int lastStateIndex = states.get(res[pos-1]);
				randval = random.nextDouble() * stateFrequencies[lastStateIndex];
				for(int selectedPos=0; selectedPos<numStates && randval > 0; selectedPos++) {
					randval -= transitions[lastStateIndex][selectedPos];
					if(randval <= 0) {
						res[pos] = revStates.get(selectedPos);
					}
				}
			}
			return new String(res);
		}
	}
	
	public static void main(String[] args) {
		int seed = new Random().nextInt();
		MarkovModel fieldModel = new MarkovModel("___*__**___*__*__*____");
		System.out.println("Simulated field:  " + fieldModel.simulate(50, seed));
		MarkovModel forestModel = new MarkovModel("**_***_********");
		System.out.println("Simulated forest: " + forestModel.simulate(50, seed));
	}
}
