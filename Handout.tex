\documentclass{article}
\usepackage[ngerman]{babel}
\usepackage[utf8]{inputenc}
\usepackage[left=2.5cm,top=2.5cm,right=2.5cm,bottom=2cm,marginparsep=0.75cm]{geometry}
\usepackage{graphicx}
\usepackage{wrapfig}
\usepackage{caption}
\usepackage{marginnote}
\usepackage{qrcode}
\usepackage{fancyhdr}
\usepackage{listings}
\usepackage{color}

\definecolor{javared}{rgb}{0.6,0,0} % for strings
\definecolor{javagreen}{rgb}{0.25,0.5,0.35} % comments
\definecolor{javapurple}{rgb}{0.5,0,0.35} % keywords
\definecolor{javadocblue}{rgb}{0.25,0.35,0.75} % javadoc
\lstset{language=Java,
%basicstyle=\ttfamily,
%basicstyle=\small,
basicstyle=\fontsize{9}{9.5}\selectfont\ttfamily,
keywordstyle=\color{javapurple}\bfseries,
stringstyle=\color{javared},
commentstyle=\color{javagreen},
morecomment=[s][\color{javadocblue}]{/**}{*/},
numbers=left,
numberstyle=\tiny\color{black},
stepnumber=1,
numbersep=10pt,
tabsize=2,
showspaces=false,
breaklines=true,
showstringspaces=false}

\newcommand{\giturl}{https://is.gd/BioInfoPWD}

\pagestyle{fancy}
\fancyhf{}
\rhead{Dr.-Ing. Piotr Wojciech Dabrowski}
\lhead{Aktuelle Entwicklungen in der Bioinformatik}
\rfoot{Seite \thepage}

\begin{document}
\reversemarginpar
\section*{Aktuelle Entwicklungen in der Bioinformatik}

\begin{wrapfigure}{r}{0.3\textwidth}
 \vspace{-48pt}
 \begin{center}
  \qrcode[height=0.28\textwidth]{\giturl}
 \end{center}
 \vspace{-10pt}
 \caption*{\giturl}
 \vspace{-45pt}
\end{wrapfigure}

In \marginnote{5} der \textbf{Bioinformatik} werden Software und Algorithmen entwickelt und auf biologische Daten angewendet, um die Daten und die darunterliegenden biologischen Zusammenhänge zu verstehen. 


Aus dieser Definition lassen sich \textbf{drei für das Verständnis der Bioinformatik wichtige Kernfragen} ableiten:
\begin{itemize}
 \item Was sind biologische Daten, wo kommen sie her, wie sehen sie aus?
 \item Welche Algorithmen werden verwendet?
 \item Was bedeutet ``Verstehen'' in diesem Zusammenhang, was sind die Kernfragen?
\end{itemize}

Für eine Behandlung der gesamten Vielfalt an Typen biologischer Daten, Fragestellungen und Algorithmen reicht die Zeit in einer 45-minütigen Vorlesung nicht, daher findet in dieser Veranstaltung eine Fokussierung auf Klinische Metagenomik statt. 

\marginnote{6}
Als Veranschaulichung der Skala dieser Fokussierung: Die bio.tools-Registry\footnote{https://bio.tools – ein Repositorium anhand der EDAM-Ontologie annotierter bioinformatischer Tools, das sich zum Ziel gesetzt hat, alle in der Bioinformatik vorhandenen Tools zu katalogisieren und zu annotieren.}  kennt ca. 12'000 bioinformatische Tools, es gibt in der Klinischen Metagenomik ca. 40 häufig verwendete Tools\footnote{Die Anzahl der Tools ist natürlich keine zuverlässige Darstellung der Größe eines Feldes, kann aber dienen, um eine Vorstellung der Größenordnung zu vermitteln.}. 

\marginnote{7}
Eine \textbf{Kernfrage der Klinischen Metagenomik} beschäftigt sich mit der Diagnostik: Welche Krankheitserreger sind in einem Patienten vorhanden, welche sind für seine Symptome verantwortlich? Aus diesen Informationen können Ärzte eine \textbf{auf den Patienten und die Erkrankung zugeschnittene Behandlung} definieren. 

Die \textbf{Motivation für die Diagnostik mittels Klinischer Metagenomik} liegt in den Schwächen anderer diagnostischer Methoden:
\begin{itemize}
 \item \textbf{Symptom-basierte Diagnostik:} Ein Arzt schätzt auf Basis der Symptome und weiterer Informationen (z.B. Reiseanamnese, Patientenakte) welcher Krankheitserreger wahrscheinlich ursächlich für die Erkrankung ist. Häufig können aber sehr unterschiedliche Erreger ähnliche Symptome hervorrufen, also ist diese Methode unsicher.
 \item \textbf{Labordiagnostik:} Eine Patienten-probe (z.B. Körperflüssigkeit, Biopsie) wird molekularbiologisch im Labor untersucht. Solche Tests sind aber erregerspezifisch: Ein Test kann das Vorhandensein eines konkreten Krankheitserregers nachweisen. Krankheitserreger, mit denen der Arzt nicht rechnet und entsprechend keinen spezifischen Nachweis anfordert, oder für die gar kein Test verfügbar ist (z.B. seltene oder mutierte Krankheitserreger) können mittels Labordiagnostik nicht nachgewiesen werden.
\end{itemize}

In der Metagenomik wird sämtliche\footnote{Das ist eine grobe Vereinfachung: In den meisten Fällen wäre die Erfassung sämtlicher DNA prohibitiv teuer, entsprechend wird nur eine zufällig ausgewählte Teilmenge der Gesamt-DNA erfasst. Diese Unterscheidung ist aber für die Detail-tiefe dieser Veranstaltung irrelevant.} DNA in einer Probe erfasst. Dies erlaubt einen offenen Blick in die Probe – ist ein Krankheitserreger in der Probe, wird seine DNA erfasst, und über diese Information kann wiederum der Krankheitserreger identifiziert werden. Die \textbf{Diagnostik mittels Klinischer Metagenomik} nutzt somit einen \textbf{offenen Blick} in die Probe und erlaubt die \textbf{unvoreingenommene Diagnose jedes Krankheitserregers}.

\marginnote{8}
Allerdings ist die Klinische Metagenomik ein recht junges Feld. Für einen standardmäßigen Einsatz im klinischen Alltag \textbf{fehlen systematische Beweise für die Zuverlässigkeit der Methode} (z.B. dass alles, was man mit der Labordiagnostik finden würde, immer auch mit Klinischer Metagenomik gefunden wird).

\marginnote{10}
Die \textbf{DNA}, welche in der Klinischen Metagenomik gemessen wird, ist der \textbf{Träger der Erbinformation}. Diese Information ist codiert in einer \textbf{Aneinanderreihung der vier Basen (A)denosin, (G)uanin, (T)hymin und (C)ytosin}, die - wie die Sprossen einer in der Mitte durchgesägten Strickleiter - über ein Rückgrat miteinander verbunden sind. In der Bioinformatik wird eine DNA-Sequenz in der Regel als einfache Textdatei mit einer entsprechenden Folge von A, G, T und C gespeichert\footnote{Das am häufigsten für eine solche einfache Repräsentation einer Sequenz verwendete Dateiformat heißt FASTA-Format.}. Die gesamte Basenabfolge, die in einem Organismus zu finden ist, wird als Genom bezeichnet. \textbf{Je nach Organismus kann das Genom sehr unterschiedliche Größen haben}: Humanpathogene Viren können Genome von ca. 3-300 KB\footnote{Kilobasen, aber praktischerweise im FASTA-Format auch die ungefähre Dateigröße in Kilobyte.} haben, Bakterien um die 1-5 MB. Das Menschliche Genom ist ca. 3 GB lang. Das größte derzeit bekannte Genom besitzt mit ca. 150 GB eine Pflanze.

Unterschiedliche Bereiche im Genom haben unterschiedliche Funktionen. Besonders prominent sind die \textbf{codierenden Regionen}\footnote{CDS (Coding Sequence)}, welche Baupläne für die Proteine darstellen, sowie die \textbf{regulatorischen Regionen}, welche definieren, unter welchen Umständen welche Baupläne umgesetzt und entsprechend wann welche Proteine gebildet werden. 

\marginnote{11}
Aufgrund ihrer chemischen Eigenschaften können von den vier Basen jeweils zwei eine Paarung eingehen: \textbf{A und T sowie G und C können aneinander binden}. Diese Paare werden jeweils als \textbf{komplementäre Basen} bezeichnet. Die DNA liegt in Zellen entsprechend nicht als einzelner Strang vor, sondern jeder Base in dem einen DNA-Strang steht eine komplementäre Base gegenüber. Somit liegen in der Zelle zwei, über die komplementären Basen miteinander wie eine Strickleiter verbundene, DNA-Stränge vor. Durch mechanische Spannungen ist dieser Doppelstrang ineinander verdreht - das ist die berühmte \textbf{DNA-Doppelhelix}. Durch die Eindeutigkeit der Basenpaarungen beschreibt die Sequenz jedes Stranges eindeutig die Sequenz des Gegenstranges, und \textbf{jeder Strang enthält sämtliche Erbinformation}.

Teilt sich eine Zelle, so wird von spezialisierten Enzymen - das sind Proteine, die bestimmte biochemische Reaktionen katalysieren\footnote{Chemisch nicht korrekt aber hilfreich ist es, sich Enzyme wie kleine Roboter vorzustellen, die jeweils eine bestimmte Aufgabe in der Zelle - z.B. das Synthetisieren eines Gegenstranges zu einem DNA-Einzelstrang - durchführen.} - die DNA-Doppelhelix in Einzelstränge aufgetrennt. Andere Enzyme, die \textbf{DNA-Polymerasen}, kopieren jeden der Einzelstränge indem sie aus komplementären Basen jeweils einen Gegenstrang aufbauen. Somit entstehen aus einem DNA-Doppelstrang zwei Kopien, die auf die zwei Tochterzellen aufgeteilt werden.

\marginnote{12}
Die Fähigkeit der \textbf{Polymerase}, DNA-Einzelstränge zu kopieren, wird \textbf{seit 1977 für die Sequenzierung einzelner DNA-Moleküle} genutzt. Heutzutage wird die Methode zur Sequenzierung einzelner DNA-Moleküle als \textbf{First Generation Sequencing} bezeichnet. Seit 2000 findet die als \textbf{Next Generation Sequencing (NGS)} bezeichnete \textbf{massive Parallelisierung} der Sequenzierung statt. Dabei werden ursprünglich hunderttausende, mittlerweile bis zu mehrere Milliarden von Sequenzierungen einzelner DNA-Moleküle parallel in einem Durchlauf eines Sequenziergeräts durchgeführt. Hier wird das Prinzip am Beispiel der Sequenziertechnologie von Illumina - derzeitiger Marktführer für NGS - erläutert. 

Zunächst wird sämtliche in einer Probe vorhandene DNA in kleine Stücke mit einer Länge von meist 100-1000 Basen zerschnitten\footnote{Das kann auf unterschiedlichen Wegen passieren. Einige Beispiele: Durch eine Behandlung mit Ultraschall, die zu Brüchen im Rückgrat der DNA führt; Die Lösung mit DNA kann durch eine schmale Düse gespritzt werden, wodurch Scherkräfte entstehen, die wiederum zu Brüchen im Rückgrat der DNA führen; Es können Enzyme hinzugegeben werden, die darauf spezialisiert sind, DNA durch Zerschneiden des Rückgrats abzubauen.}. Danach werden die DNA-Doppelstränge chemisch in Einzelstränge aufgetrennt.

\marginnote{13}
Die Lösung mit den DNA-Einzelsträngen wird dann über ein Glas-Slide gespült, auf dem Bindemoleküle verteilt sind, an denen die Einzelstränge kleben bleiben können. Durch einen Wasch-schritt werden diejenigen DNA-Einzelstränge, die an keiner Bindungsstelle hängen geblieben sind, entfernt. 

\marginnote{14}
Danach wird die aus der DNA-Replikation in der Zellteilung bekannte \textbf{Polymerase} hinzugegeben. \textbf{Die Sequenzierung erfolgt} dann \textbf{Zyklisch}: 
\begin{enumerate}
    \item \textbf{Es werden Basen}\footnote{Korrekt wäre: dNTPs, das sind die eigentlichen Bauteile für die DNA, die aus der Base und einem Rückgrat-Teil bestehen, aber der Einfachheit halber wird dieses Detail hier nicht weiter betrachtet.} \textbf{hinzugefügt}, die aber chemisch modifiziert sind: Sie enthalten eine \textbf{fluoreszente Markierung}, die für jede der 4 Basen eine andere Farbe hat und so angebracht ist, dass die Polymerase dahinter keine weiteren Basen mehr einbauen kann
    \item \textbf{Die Polymerase baut} in jedem der am Glas-Slide hängenden DNA-Einzelstränge \textbf{eine} zur nächsten freien Base komplementäre \textbf{fluoreszent markierte Base} ein
    \item Es wird ein \textbf{Bild des Glas-Slides} aufgenommen, auf dem die fluoreszenten Markierungen der gerade eingebauten Basen als leuchtende Punkte erkennbar sind
    \item Die nicht eingebauten Basen werden weg gespült
    \item Die \textbf{fluoreszenten Markierungen} an den frisch eingebauten Basen werden chemisch \textbf{abgespalten}, so dass die Polymerase nun weitere Basen einbauen kann
\end{enumerate}

Durch eine Wiederholung dieser Zyklen entsteht eine Abfolge von Bildern, auf denen die Positionen der am Slide gebundenen DNA-Einzelstränge als leuchtende Punkte erkennbar sind. An der \textbf{Abfolge der Farben dieser Punkte} auf den aufeinander folgenden Bildern wird abgelesen, welche Basen die Polymerase bei der Vervielfältigung jedes dieser Einzelstränge zu welchem Zeitpunkt eingebaut hat. Somit kann \textbf{für jeden der Einzelstränge die Sequenz rekonstruiert} werden\footnote{Diese Rekonstruktion ist eine komplexe Aufgabe der Bilddatenverarbeitung - nur eins von vielen Beispielen für die Interdisziplinarität der Bioinformatik.}. 

Die finale Ausgabe einer solchen Sequenzierung ist eine Textdatei, in der bis zu mehrere Milliarden Sequenzen von in der Regel 100 bis 300 Basen Länge stehen. Diese kurzen Sequenzen werden als \textbf{Reads} bezeichnet.

Die beschriebene Vorgehensweise - Fragmentieren der DNA gefolgt von der parallelen Sequenzierung einer riesigen Anzahl von kurzen DNA-Stücken - wird als \textbf{Second Generation Sequencing} bezeichnet und ist aktuell die am meisten eingesetzte NGS-Technologie.

\marginnote{15}
Seit wenigen Jahren sind zudem \textbf{Third Generation Sequencing}-Geräte auf dem Markt verfügbar. Diese erlauben es, weniger aber dafür deutlich längere Reads zu generieren. Ein solches Produkt ist der \textbf{MinION}. Im MinION sind zwei \textbf{Pufferlösungen} mit \textbf{unterschiedlichen Konzentrationen} elektrisch geladener Moleküle (\textbf{Ionen}) durch eine Membran getrennt, somit besteht eine elektrische Spannung zwischen den beiden Seiten der Membran. In der Membran eingelassen sind Kanäle, durch die Ionen fließen können - es entsteht ein elektrischer Strom, der an jedem der Kanäle gemessen wird. Des Weiteren sind \textbf{auf den Kanälen Enzyme befestigt}, die einen DNA-Doppelstrang auftrennen und einen der \textbf{DNA-Einzelstränge durch den Kanal schieben} können. \textbf{Abhängig von der im Kanal befindlichen Base ändert sich} aufgrund der unterschiedlichen Größe der einzelnen Basen der im Kanal für die Ionen verfügbare Platz und somit der elektrische Widerstand des Kanals und \textbf{der durch diesen fließende elektrische Strom}. Anhand der gemessenen Änderungen des Stroms wird die Folge der durch den Kanal geschobenen Basen rekonstruiert. Dabei können Reads mit einer Länge von bis zu 20'000 Basen gelesen werden. 

Eine besondere Eigenschaft des \textbf{MinION} ist dabei seine Größe: Er ist \textbf{kaum größer als ein USB-Stick}, kann per USB an jeden Laptop oder PC angeschlossen werden und passt zusammen mit allen benötigten Chemikalien und Utensilien in das Handgepäck. Diese Mobilität erlaubt neue flexible Anwendungen. Beispielsweise wurden während des \textbf{Ebola-Ausbruchs} in Westafrika in einigen Zentren positive \textbf{Patientenproben direkt vor Ort sequenziert} und durch die darauf basierende \textbf{Untersuchung der Mutationen des Erregers} eine \textbf{echtzeit-Rekonstruktion der Entwicklung des Ausbruchs} erstellt. Ähnlich wurde während des \textbf{Zikavirus-Ausbruchs} in Lateinamerika \textbf{in einem Bus ein mobiles Labor} eingerichtet, das durch Brasilien fuhr und ebenfalls in \textbf{Echtzeit Untersuchungen zu der Verbreitung} unterschiedlicher Varianten durchführte\footnote{ZIBRA project, http://www.zibraproject.org/}.

Während mit Third Generation Sequencing allgemein viel \textbf{längere Reads} generiert werden können als mit Second Generation Sequencing, sind die \textbf{Fehlerraten auch deutlich höher}: Während normale Illumina-Reads ca. 0.1$\%$ Fehler enthalten, sind in MinION-Reads ca. 10$\%$ der Basen fehlerhaft. Somit bietet Third Generation Sequencing \textbf{neue Möglichkeiten} bringt aber auch \textbf{neue Herausforderungen} und treibt die Entwicklung in der Bioinformatik mit \textbf{neuen Anforderungen an Algorithmen und neuen Fragestellungen} voran.

\marginnote{16}
In der \textbf{Klinischen Metagenomik} wird aufgrund der für die Fragestellung vorteilhaften großen Read-Anzahl aktuell \textbf{primär Second Generation Sequencing verwendet}. Die \textbf{Identifikation von Krank\-heits\-er\-re\-gern} kann am einfachsten durch \textbf{Vergleiche der Reads} mit bekannten Genomsequenzen unterschiedlicher Organismen - \textbf{Referenzsequenzen} - erfolgen. Kann ein Read eindeutig der Referenzsequenz eines Krankheitserregers zugeordnet werden, ist das ein starker Hinweis auf das Vorhandensein dieses Krankheitserregers in der Probe. Um \textbf{Sequenzierfehlern und Mutationen} Rechnung zu tragen, ist dabei ein exaktes string matching nicht ausreichend, es muss auf \textbf{unscharfe Suche} (approximate string search) zurückgegriffen werden. Eine der wichtigsten Datenbanken für Referenzsequenzen, \textbf{GenBank, umfasst derzeit ca. 210 Millionen Referenzsequenzen mit ca. 270 Milliarden Basen}. \marginnote{17}Ein \textbf{paarweiser unscharfer Vergleich} von beispielsweise \textbf{2 Milliarden Reads je 200 Basen} aus einer Illumina-Sequenzierung mit allen Sequenzen aus GenBank würde auf einem handelsüblichen PC \textbf{mehrere Millionen Jahre} dauern. Entsprechend sind für die Klinische Metagenomik andere Herangehensweisen notwendig.

\marginnote{18\\Demo}
Eine Herangehensweise ist die Verwendung von \textbf{Markov-Modellen}. Ein Markov-Modell beschreibt die möglichen Zustände eines Systems (z.B. bei DNA die 4 Basen A, G, T und C) sowie die Übergangswahr\-schein\-lich\-kei\-ten zwischen den Zuständen (also z.B. dass in einer Sequenz nach einem A ein G kommt). Ein solches Modell kann an einer Sequenz trainiert werden, indem alle beobachteten Zustandsübergänge gezählt werden (beispielsweise wäre in der Sequenz AAAAG die Übergangs\-wahr\-schein\-lich\-keit von A zu A 75$\%$ und die von A zu G 25$\%$). Entsprechend kann die Wahrscheinlichkeit, dass eine Sequenz zu einem trainierten Modell passt, berechnet werden, indem die in dem Modell gespeicherten Wahrscheinlichkeiten für die in der Sequenz beobachteten Zustandsübergänge miteinander multipliziert werden.

Eine häufig in der Bioinformatik verwendete Weiterentwicklung des Markov-Modells ist das \textbf{Hidden Markov Model (HMM)}. Das \textbf{Markov-Modell} geht davon aus, dass die Zustände zwischen denen ein System wechselt (bei einer DNA-Sequenz: A, G, T und C), \textbf{sichtbar} sind. Ein \textbf{HMM} hingegen geht davon aus, dass diese beobachteten Zustände eigentlich nur Symbole sind, die mit bestimmten Wahrscheinlichkeiten von den eigentlichen, \textbf{nicht direkt beobachtbaren Zuständen} des Systems (z.B. ``regulatorische Region'' oder ``codierende Region'') emittiert werden. Die Zustandsübergänge finden auf Ebene der nicht beobachtbaren Zustände statt\footnote{Das Trainieren und Nutzen eines HMM ist deutlich komplexer als bei einem einfachen Markov-Modell. Dafür verwendete Algorithmen sind der Baum-Welch-Algorithmus und der Viterbi-Algorithmus, deren Erklärung aber den Rahmen sprengen würde.}.

\marginnote{19}
Mit Hilfe von \textbf{(Hidden) Markov-Modellen kann die o.g. unscharfe Zuordnung von Reads massiv beschleunigt werden}. Dabei gibt es unterschiedliche Ausgangslagen, zu denen aktuell entsprechende Neuentwicklungen stattfinden:
\begin{itemize}
    \item \textbf{Der Krankheitserreger ist bekannt, aber die Details (z.B. Antibiotika-Resistenzen, genetische Modifikationen) sind unbekannt.} In diesem Fall können zwei Markov-Modelle trainiert werden: Ein Modell A auf dem menschlichen Genom, ein Modell B auf allen bekannten Referenzgenomen des Erregers. Dann wird für jeden Read die Wahrscheinlichkeit berechnet, dass er zu Modell A passt, sowie dass er zu Modell B passt. Je nach dem, welche der Wahrscheinlichkeiten höher ist, wird der Read als aus dem Menschen oder aus dem Erreger stammend klassifiziert. Danach können die Erreger-Reads mit aufwändigeren Methoden eingehend untersucht werden. Beispiele sind der EHEC-Ausbruch (Rekonstruktion des EHEC-Genoms, um die für das Hämolytisch-urämische Syndrom zuständigen Gene zu finden), oder die Untersuchung eines potentiell illegal für die Tumortherapie eingesetzten Vaccinia-Virus (Rekonstruktion des viralen Genoms, um nach illegalen genetischen Modifikationen zu suchen).
    \item \textbf{Der Krankheitserreger ist unbekannt, aber es liegt ein Referenzgenom eines verwandten Erregers in der Datenbank vor.} Eine aktuelle Herangehensweise, um in so einem Fall schnell und sensitiv auch stark mutierte Viren zu finden, ist aus allen bekannten viralen Referenzgenomen die codierenden Regionen zu clustern und auf jedem Cluster ein HMM zu trainieren. Jeder Read wird dann mit jedem HMM verglichen. Wird für einen Read ein HMM gefunden, das eine deutlich höhere Wahrscheinlichkeit als alle anderen HMMs aufweist und bei dem die Wahrscheinlichkeit einen Schwellenwert überschreitet, wird der Read als viral klassifiziert. 
    \item \textbf{Der Krankheitserreger ist bisher nicht beschrieben.} Dies stellt eine sehr große Herausforderung dar, weil es kein Erregergenom gibt, mit dem man Reads vergleichen kann. Für so einen Fall wurde ein Tool entwickelt, welches HMMs auf allen codierenden Regionen trainiert, die ausschließlich in den Menschen infizierenden Krankheitserregern gefunden wurden. Wie im vorherigen Punkt beschrieben werden diese HMMs auf alle Reads angewendet, um diejenigen Reads zu identifizieren, welche auf Gefahren für den Menschen hinweisen könnten.
\end{itemize}

\marginnote{20}
Neben Aussagen zu Krankheitserregern lassen sich aus metagenomischen Datensätzen auch \textbf{Aussagen über die Patienten} herleiten. Diese können zum Teil \textbf{sehr persönlicher Natur} sein: Veranlagung für Erbkrankheiten, Abstammung, aber auch HIV-Status oder sexuelle Praktiken. Entsprechend ist es sehr wichtig, eine \textbf{ethische Vorgehensweise} bei der Bearbeitung solcher Daten und der Auswahl der durchzuführenden Auswertungen zu beachten. Auch der \textbf{Datenschutz und die Datensicherheit} spielen eine wichtige Rolle. Das Bewusstsein für die Wichtigkeit von Themen wie das \textbf{Recht auf Nichtwissen} oder den \textbf{Schutz personenbezogener Daten} sind in der Klinischen Metagenomik in großen Teilen der Community zwar noch nicht besonders stark ausgeprägt, aber der steigende Stellenwert solcher Fragestellungen wird durch eine steigende Anzahl von Publikationen und Fachvorträgen zu diesen Themen deutlich.

\newpage
\thispagestyle{empty}
\newgeometry{top=0.5in, left=0.8in, right=0.5in, bottom=0.5in}
\lstinputlisting{MarkovModelExample.java}

\end{document}
